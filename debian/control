Source: ska
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               zlib1g-dev,
               googletest <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/ska
Vcs-Git: https://salsa.debian.org/med-team/ska.git
Homepage: https://github.com/simonrharris/SKA
Rules-Requires-Root: no

Package: ska
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Split Kmer Analysis
 SKA (Split Kmer Analysis) is a toolkit for prokaryotic (and any other
 small, haploid) DNA sequence analysis using split kmers. A split kmer is
 a pair of kmers in a DNA sequence that are separated by a single base.
 Split kmers allow rapid comparison and alignment of small genomes, and
 is particulalry suited for surveillance or outbreak investigation. SKA
 can produce split kmer files from fasta format assemblies or directly
 from fastq format read sequences, cluster them, align them with or
 without a reference sequence and provide various comparison and summary
 statistics. Currently all testing has been carried out on high-quality
 Illumina read data, so results for other platforms may vary.
